import Vuex from 'vuex';
import auth from './modules/auth';
// import {getCookie} from '../helpers/dataHelper';
import message from './modules/message';
import user from './modules/user';
// eslint-disable-next-line sort-imports
import axios from 'axios';

export default function() {
    return new Vuex.Store({
        state: {
            authUser: null
        },
        mutations: {
            SET_USER(state, user) {
                state.authUser = user;
            }
        },
        actions: {
            nuxtServerInit({commit}, {req}) {
                if (req.session && req.session.authUser)
                    commit('SET_USER', req.session.authUser);
            },
            async login({commit}, {username, password}) {
                try {
                    const {data} = await axios.post('/api/login', {
                        username,
                        password
                    });
                    commit('SET_USER', data);
                }
                catch (error) {
                    if (error.response && error.response.status === 401)
                        throw new Error('You need to log in first');
                    throw error;
                }
            },
            async logout({commit}) {
                await axios.post('/api/logout');
                commit('SET_USER', null);
            }
        },
        modules: {
            user,
            auth,
            message
        }
    });
}
